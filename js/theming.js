$("input:radio[name=radio-navbar]").bind("click", function() {
  var value;
  value = $(this).val();
  if (value === "default") {
    return $("#navbar").addClass("navbar-default").removeClass("navbar-inverse");
  } else if (value === "inverse") {
    return $("#navbar").removeClass("navbar-default").addClass("navbar-inverse");
  }
});
